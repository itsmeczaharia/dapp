// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;
pragma abicoder v2;


contract Dissertation {
    enum ProjectStatuses {CREATED, LIVE, FINISHED, SUCCESS, REFUNDED}

    string private TRENDING = "Trending";
    string private NEARLY_FUNDED = "Nearly Funded";
    string private JUST_LAUNCHED = "Just Launched";
    string private UPCOMING_PROJECTS = "Upcoming Projects";
    string private EVERYTHING = "Everything";

    uint256 public numTags;
    uint256 public numCategories;
    uint256 public numProjects;
    uint256 public numFunds;

    mapping(uint256 => Tag) public tags;
    mapping(uint256 => Category) public categories;
    mapping(uint256 => Project) public projects;
    mapping(uint256 => Fund) public funds;
    mapping(uint256 => address[]) public reacts;

    constructor() {
        numProjects = 0;
        numTags = 0;
        numCategories = 0;

        createCategory("Art");
        createCategory("Comics");
        createCategory("Crafts");
        createCategory("Food");
        createCategory("Games");
        createCategory("Music");
        createCategory("Technology");

        createProject("Gravity: The Weighted Blanket for Sleep, Stress and Anxiety", "A weighted blanket engineered to be 10% of your body weight to naturally reduce stress and increase relaxation.", 1625348139, 39 ether, 3);
        createProject("Darkest Dungeon: The Board Game", "Gather heroes, brave the darkness and face the lurking abominations in this cooperative 1-4 player rogue-like RPG dungeon crawler!", 1625348139, 72 ether, 4);
        createProject("Zombicide: Green Horde", "The medieval fantasy Zombicide saga continues with a horde of undead orcs, new environment features, resources and challenges!", 1625348139, 39 ether, 5);
        createProject("Pebble 2, Time 2 + All-New Pebble Core", "Two affordable, heart rate-enabled smartwatches and a hackable, 3G wearable for phone-free running with GPS, music, and Amazon Alexa.", 1625348139, 42 ether, 3);
        createProject("OYO NOVA Gym", "A FULL GYM IN YOUR HANDS: Transform your Body at Home, Office &amp; On the Go. Lose the Weights, Keep the Resistance.", 1625348139, 13 ether, 1);
        createProject("Kingdom Death: Monster 1.5", "A cooperative nightmare horror game experience. Hunt intelligent monsters and develop your settlement through a self-running campaign.", 1625348139, 65 ether, 1);
        createProject("Everdell: Newleaf, Mistwood, and The Complete Collection", "Return to Everdell with 2 expansions (Newleaf &amp; Mistwood), a Big Ol' Box of Storage, and Accessories. Or get The COMPLETE Collection!", 1625348139, 58 ether, 4);
        createProject("The Way of Kings 10th Anniversary Leatherbound Edition", "Celebrate 10 years of The Stormlight Archive with Brandon Sanderson", 1625348139, 20 ether, 2);
        createProject("Bird Buddy: A Smart Bird Feeder", "Bird Buddy notifies you of feathered visitors, captures their photos and organizes them in a beautiful collection!", 1625348139, 44 ether, 0);
        createProject("Otto's G32  the first smart and modular gas grill.", "The G32 revolutionizes gas grilling and perfectly adjusts to your grilling needs", 1625348139, 48 ether, 1);
        createProject("Flipper Zero  Multi-tool Device for Hackers", "Open source multi-tool device for researching and pentesting radio protocols, access control systems, hardware, and more.", 1625348139, 67 ether, 4);
        createProject("1982: GREATEST GEEK YEAR EVER!", "An epic documentary celebrating the 40th anniversary of some of the greatest movies ever made!", 1625348139, 64 ether, 3);
        createProject("Bring Reading Rainbow Back for Every Child, Everywhere!", "Bring Reading Rainbows library of interactive books &amp; video field trips to more platforms &amp; provide free access to classrooms in need!", 1625348139, 77 ether, 6);
        createProject("Bloodstained: Ritual of the Night", "IGAVANIA (n): A gothic, exploration-focused action platformer, designed by one of the godfathers of the genre!", 1625348139, 43 ether, 3);
        createProject("Splinter: A Short Thriller", "An original short thriller that examines the moral question: Do the needs of the many indeed outweigh the needs of the few? Or the one?", 1625348139, 32 ether, 3);
        createProject("Batman: Gotham City Chronicles", "Fight for Gotham City as the heroes or work alone as the villain in a miniatures boardgame for 2 to 4 players exclusive to Kickstarter.", 1625348139, 47 ether, 2);
        createProject("Critical Role: The Legend of Vox Machina Animated Special", "Critical Role's The Legend of Vox Machina reunites your favorite heroes for a professional-quality animated special!", 1625348139, 21 ether, 5);
        createProject("Dark Souls - The Board Game", "Dark Souls - The Board Game is a strategically challenging, deeply immersive combat exploration game for 1-4 players.", 1625348139, 26 ether, 1);
        createProject("The Veronica Mars Movie Project", "UPDATED: This is it. We're making a Veronica Mars movie! Now the only question is: how big can we make it? We need your help!", 1625348139, 77 ether, 2);
        createProject("Marvel United: X-Men", "Marvel United returns with the X-Men! Bringing brand-new features to the acclaimed game, new Heroes, Villains, and amazing pieces!", 1625348139, 58 ether, 2);
        createProject("terraplanter - visibly follow the journey from seed to plant", "Just fill with water and the plant will take only what it needs to visibly grow &amp; flourish on the exterior surface of the planter.", 1625348139, 11 ether, 1);
        createProject("The Lark's Call: Irish Traditional music on uilleann pipes.", "Tom Delany's debut solo album", 1625348139, 11 ether, 4);
        createProject("Bring Back MYSTERY SCIENCE THEATER 3000", "Almost there! MYSTERY SCIENCE THEATER 3000 will return... and if we can get to $5.9MM, we'll get THIRTEEN new episodes of MST3K!", 1625348139, 85 ether, 0);
        createProject("The Witcher: Old World", "Action-packed adventure board game, full of immersive choices and excitingly fresh mechanics.", 1625348139, 49 ether, 4);
        createProject("The Wyrmwood Modular Gaming Table: Coffee &amp; Dining Models", "A revolutionary table that evolves over a lifetime. Innovative, yet affordable, with magnetic accessories. Crafted without compromise.", 1625348139, 36 ether, 0);
        createProject("Fidget Cube: A Vinyl Desk Toy", "An unusually addicting, high-quality desk toy designed to help you focus. Fidget at work, in class, and at home in style.", 1625348139, 76 ether, 0);
        createProject("Monster Hunter World: The Board Game", "The ultimate tabletop monster hunting experience! Cooperative arena combat board game based on the bestselling video game ", 1625348139, 76 ether, 0);
        createProject("Frosthaven", "Euro-inspired dungeon crawling sequel to the 2017 smash-hit Gloomhaven", 1625348139, 91 ether, 4);
        createProject("Exploding Kittens", "This is a card game for people who are into kittens and explosions and laser beams and sometimes goats.", 1625348139, 92 ether, 6);
        createProject("The Everyday Backpack, Tote, and Sling", "The world's best everyday bags. Designed by photographers to revolutionize camera carry, but built for everyone to organize your life.", 1625348139, 38 ether, 6);
        createProject("Xeric Apollo 11 50th Anniversary Automatic Watch", "A glowing solar system on your wrist! A collection of watches to celebrate the 50th anniversary of the NASA Apollo 11 moon landing.", 1625348139, 19 ether, 6);
        createProject("Redefining Premium Watches - Filippo Loreti", "Your Last Chance - Join The World's Most Funded Timepiece Project In Kickstarter History!", 1625348139, 32 ether, 2);
        createProject("Good Boy #1: Old Dog", "They killed his human. He wants revenge.", 1625348139, 60 ether, 4);
        createProject("OUYA: A New Kind of Video Game Console", "Cracking open the last closed platform: the TV.  A beautiful, affordable console -- built on Android, by the creator of Jambox.", 1625348139, 16 ether, 6);
        createProject("Pebble: E-Paper Watch for iPhone and Android", "Pebble is a customizable watch. Download new watchfaces, use sports and fitness apps, get notifications from your phone.", 1625348139, 65 ether, 3);
        createProject("The Travel Line: Versatile Travel Backpack + Packing Tools", "Luggage redefined: a carry-on Travel Backpack and system of Packing Tools designed around the idea that no two trips are the same.", 1625348139, 21 ether, 4);
        createProject("Nemesis Lockdown", "Stand alone expansion to one of the biggest Board Game hit of recent years, Nemesis", 1625348139, 69 ether, 0);
        createProject("Etherfields", "Join first Dream Crawler - fresh co-operative Board Game experience with unique art and mysterious story waiting to be discovered.", 1625348139, 100 ether, 5);
        createProject("COOLEST COOLER: 21st Century Cooler that's Actually Cooler", "The COOLEST is a portable party disguised as a cooler, bringing blended drinks, music and fun to any outdoor occasion.", 1625348139, 37 ether, 1);
        createProject("Emily Hare's Mini Book Of Monsters", "An art book filled with a collection of mischievous creatures", 1625348139, 22 ether, 2);
        createProject("Pono Music - Where Your Soul Rediscovers Music", "Pono's mission is to provide the best possible listening experience of your favorite digital music.", 1625348139, 24 ether, 5);
        createProject("The Everyday Messenger: A Bag For Cameras &amp; Essential Carry", "Beautiful, intelligent, adaptable. The Everyday Messenger is more than just an innovative camera bag. Its a giant leap for bag-kind.", 1625348139, 82 ether, 0);
        createProject("Eiyuden Chronicle: Hundred Heroes", "An epic experience from the creators of classic JRPGs, with an intricate story and a gigantic cast of characters!", 1625348139, 15 ether, 4);
        createProject("Travel Tripod by Peak Design", "A full-featured tripod in a truly portable form.", 1625348139, 27 ether, 5);
        createProject("TRAVEL JACKETS with 15 Features by BAUBAX", "TRAVEL JACKET with built-in Neck Pillow, Eye Mask, Gloves, Earphone Holders, Drink Pocket, Tech Pockets of all sizes! Comes in 4 Styles", 1625348139, 31 ether, 4);
        createProject("LaserPecker 2-Super Fast Handheld Laser Engraver &amp; Cutter", "Ultra-fast 5W laser engraver &amp; cutter for art, business &amp; leisure use", 1625348139, 92 ether, 4);
        createProject("Shenmue 3", "Yu Suzuki presents the long awaited third installment in the Shenmue series.", 1625348139, 68 ether, 0);
        createProject("Pebble Time - Awesome Smartwatch, No Compromises", "Color e-paper smartwatch with up to 7 days of battery and a new timeline interface that highlights what's important in your day.", 1625348139, 15 ether, 1);
        createProject("Tainted Grail: The Fall of Avalon", "Adventure, survival co-op Board Game set in unique grim world inspired by Arthurian Legends. Unforgettable experience for 1-4 players", 1625348139, 23 ether, 6);
        createProject("Snapmaker 2.0: Modular 3-in-1 3D Printers", "Unlock your full creative potential from 3D printing to laser engraving, cutting and CNC carving. Smarter, larger, and more powerful.", 1625348139, 89 ether, 1);
        createProject("ZeTime: World's first smartwatch with hands over touchscreen", "Proudly designed in Switzerland, the perfect always-on smartwatch blending classic design and smart features at an affordable price", 1625348139, 100 ether, 2);
        createProject("Let's Make More MST3K &amp; Build The Gizmoplex!", "Together, let's make more MYSTERY SCIENCE THEATER 3000 *without* a network + BUILD THE GIZMOPLEX, an online theater for live events!", 1625348139, 69 ether, 3);

        createTag(TRENDING);
        createTag(NEARLY_FUNDED);
        createTag(JUST_LAUNCHED);
        createTag(UPCOMING_PROJECTS);
        createTag(EVERYTHING);

        addReact(0);
        addReact(1);
        addReact(2);
        addReact(3);
        addReact(4);
        addReact(5);
        addReact(6);
        addReact(7);
        addReact(8);
        addReact(9);
        addReact(10);
        addReact(11);
    }

    struct Fund {
        address supporterAddress;
        uint256 projectId;
        uint256 amount;
    }

    struct Tag {
        uint256 tagId;
        string tagName;

        uint256[] projects;
    }

    struct Category {
        uint256 categoryId;
        string categoryName;

        uint256[] projects;
    }

    struct Project {
        uint256 projectId;
        string projectName;
        string projectDescription;
        address creator;

        uint256 backers;
        uint256 launchDate;
        uint256 deadline;
        uint256 target;
        uint256 current;

        uint256 category;
        uint256 totalReacts;

        ProjectStatuses status;
    }

    function addReact(uint256 projectId) public {
        int256 index = - 1;
        uint256 length = reacts[projectId].length;

        for (uint256 i = 0; i < length; i++) {
            if (reacts[projectId][i] == msg.sender) {
                index = int256(i);
                break;
            }
        }

        if (index != - 1) return;
        reacts[projectId].push(msg.sender);
        projects[projectId].totalReacts++;
    }

    function getReacts(uint256 projectId) public view returns (address[] memory){
        return reacts[projectId];
    }

    function removeReact(uint256 projectId) public {
        int256 index = - 1;
        uint256 length = reacts[projectId].length;

        for (uint256 i = 0; i < length; i++) {
            if (reacts[projectId][i] == msg.sender) {
                index = int256(i);
                break;
            }
        }

        if (index == - 1) return;
        reacts[projectId][uint256(index)] = reacts[projectId][length - 1];
        reacts[projectId].pop();
        projects[projectId].totalReacts--;
    }

    function createCategory(string memory categoryName) public {
        categories[numCategories] = Category(numCategories, categoryName, new uint256[](0));
        numCategories++;
    }

    function createTag(string memory tagName) public {
        tags[numTags] = Tag(numTags, tagName, new uint256[](0));
        numTags ++;
    }

    function getCategories() public view returns (Category[] memory){
        Category[] memory categoriesToReturn = new Category[](numCategories);

        for (uint256 i = 0; i < numCategories; i++) {
            categoriesToReturn[i] = categories[i];
        }

        return categoriesToReturn;
    }

    function getTags() public view returns (Tag[] memory){
        Tag[] memory tagsToReturn = new Tag[](numTags);

        for (uint256 i = 0; i < numTags; i++) {
            tagsToReturn[i] = tags[i];
        }

        return tagsToReturn;
    }

    function getTagsForProject(uint256 projectId) public view returns (string[] memory){
        uint256 currentIdx = 0;

        string[] memory tempTags = new string[](numTags);
        Project memory p = projects[projectId];

        if (p.backers > 100) {
            tempTags[currentIdx] = TRENDING;
            currentIdx ++;
        }

        if ((p.current * 100 / p.target) >= 80) {
            tempTags[currentIdx] = NEARLY_FUNDED;
            currentIdx ++;
        }

        if (p.status == ProjectStatuses.CREATED) {
            tempTags[currentIdx] = UPCOMING_PROJECTS;
            currentIdx ++;
        }

        if (block.timestamp - p.launchDate < 1 days) {
            tempTags[currentIdx] = JUST_LAUNCHED;
            currentIdx ++;
        }

        // Trim the "" items
        string[] memory tagsToReturn = new string[](currentIdx);
        for (uint256 j = 0; j < tagsToReturn.length; j++) {
            tagsToReturn[j] = tempTags[j];
        }

        return tagsToReturn;
    }

    function getAllProjects() public view returns (Project[] memory){
        Project[] memory projectsToReturn = new Project[](numProjects);

        for (uint256 i = 0; i < numProjects; i++) {
            projectsToReturn[i] = projects[i];
        }

        return projectsToReturn;
    }

    function getCategoryProjects(uint256 categoryId) public view returns (Project[] memory){
        uint256 goodProjects = 0;
        Project[] memory tempProjects = new Project[](numProjects);

        for (uint256 i = 0; i < numProjects; i++) {
            if (projects[i].category == categoryId) {
                tempProjects[i] = projects[i];
                goodProjects++;
            }
        }

        // Trim the "" items
        uint256 index = 0;
        Project[] memory projectsToReturn = new Project[](goodProjects);
        for (uint256 j = 0; j < numProjects; j++) {
            if (tempProjects[j].creator != address(0x0)) {
                projectsToReturn[index] = tempProjects[j];
                index ++;
            }
        }
        return projectsToReturn;
    }

    function createProject(
        string memory projectName, string memory projectDescription,
        uint256 deadline, uint256 target, uint256 category) public returns (uint256){

        Project memory p = Project(
            numProjects, projectName, projectDescription, msg.sender, 0, block.timestamp, deadline,
            target, 0, category, 0, ProjectStatuses.CREATED
        );

        projects[numProjects] = p;
        categories[category].projects.push(numProjects);
        numProjects ++;

        return numProjects - 1;
    }

    function deleteProject(uint256 projectId) public {
        require(projectId >= 0 && projectId < numProjects);

        Project memory p = projects[projectId];

        require(p.creator != address(0x0));
        require(p.creator == msg.sender);

        int256 index = - 1;
        uint256 length = categories[p.category].projects.length;

        for (uint256 i = 0; i < length; i++) {
            if (categories[p.category].projects[i] == p.projectId) {
                index = int256(i);
                break;
            }
        }

        if (index == - 1) return;
        categories[p.category].projects[uint256(index)] = categories[p.category].projects[length - 1];
        categories[p.category].projects.pop();

        removeReact(projectId);
        refundFunds(projectId);

        for (uint256 i = 0; i < numFunds; i++) {
            if (funds[i].projectId == projectId) {
                // easiest way is to just mark the Fund as an empty one
                funds[i] = Fund(address(0x0), 0, 0);
            }
        }

        delete projects[projectId];
        numProjects--;
    }

    function getFund(address supporter, uint256 projectId) private returns (uint256){
        // This returns the index of a fund. IF the fund is missing, a new fund is added to funds
        for (uint256 i = 0; i < numFunds; i++) {
            if (funds[i].supporterAddress == supporter && funds[i].projectId == projectId) {
                return i;
            }
        }
        Fund memory f = Fund(supporter, projectId, 0);
        funds[numFunds] = f;

        uint256 currentFundId = numFunds;
        numFunds ++;

        return currentFundId;
    }

    function supportProject(uint256 projectId) external payable returns (Project memory){
        require(projectId >= 0 && projectId < numProjects);
        require(projects[projectId].status == ProjectStatuses.LIVE);

        projects[projectId].current += msg.value;

        uint256 fundId = getFund(msg.sender, projectId);
        if (funds[fundId].amount == 0) {
            // count a user only at his first payment
            projects[projectId].backers ++;
        }
        funds[fundId].amount += msg.value;

        return projects[projectId];
    }

    function changeProjectStatus(uint256 projectId, uint256 statusId) public {
        require(projectId >= 0 && projectId < numProjects);
        require(projects[projectId].creator == msg.sender);
        require(projects[projectId].status != ProjectStatuses(statusId));

        projects[projectId].status = ProjectStatuses(statusId);
    }

    function getPopularProjects() public view returns (Project[] memory){
        uint256 goodProjects = 0;
        Project[] memory tempProjects = new Project[](numProjects);

        for (uint256 i = 0; i < numProjects; i++) {
            if (reacts[i].length > 0) {
                tempProjects[i] = projects[i];
                goodProjects++;
            }
        }

        // Trim the "" items
        uint256 index = 0;
        Project[] memory projectsToReturn = new Project[](goodProjects);
        for (uint256 j = 0; j < numProjects; j++) {
            if (tempProjects[j].creator != address(0x0)) {
                projectsToReturn[index] = tempProjects[j];
                index ++;
            }
        }
        return projectsToReturn;
    }

    function getUserProjects(address user) public view returns (Project[] memory){
        uint256 goodProjects = 0;
        Project[] memory tempProjects = new Project[](numProjects);

        for (uint256 i = 0; i < numProjects; i++) {
            if (projects[i].creator == user) {
                tempProjects[i] = projects[i];
                goodProjects++;
            }
        }

        // Trim the the empty
        uint256 index = 0;
        Project[] memory projectsToReturn = new Project[](goodProjects);
        for (uint256 j = 0; j < numProjects; j++) {
            if (tempProjects[j].creator != address(0x0)) {
                projectsToReturn[index] = tempProjects[j];
                index ++;
            }
        }
        return projectsToReturn;
    }

    function getUserSupportedProjects(address user) public view returns (Project[] memory){
        uint256 numGoodFunds = 0;
        Fund[] memory goodFunds = new Fund[](numFunds);

        for (uint256 i = 0; i < numFunds; i++) {
            if (funds[i].supporterAddress == user) {
                goodFunds[i] = funds[i];
                numGoodFunds ++;
            }
        }

        // Trim the the empty
        uint256 index = 0;
        Project[] memory projectsToReturn = new Project[](numGoodFunds);
        for (uint256 j = 0; j < numFunds; j++) {
            if (goodFunds[j].supporterAddress != address(0x0)) {
                projectsToReturn[index] = projects[goodFunds[j].projectId];
                index ++;
            }
        }
        return projectsToReturn;
    }

    function refundFunds(uint256 projectId) public payable returns (Project memory){
        require(projectId >= 0 && projectId < numProjects);
        require(projects[projectId].status == ProjectStatuses.LIVE);

        for (uint256 i = 0; i < numFunds; i++) {
            if (funds[i].projectId == projectId && funds[i].supporterAddress != address(0x0)) {
                (bool success,) = funds[i].supporterAddress.call{value : funds[i].amount}("");
                require(success, "Transfer failed.");
            }
        }

        projects[projectId].status = ProjectStatuses.REFUNDED;
        return projects[projectId];
    }

    function reclaimFunds(uint256 projectId) external payable returns (Project memory){
        require(projectId >= 0 && projectId < numProjects);
        require(projects[projectId].status == ProjectStatuses.LIVE);
        require(block.timestamp >= projects[projectId].deadline);

        uint256 fundsToReclaim = 0;
        for (uint256 i = 0; i < numFunds; i++) {
            if (funds[i].projectId == projectId && funds[i].supporterAddress != address(0x0)) {
                fundsToReclaim += funds[i].amount;
            }
        }
        require(projects[projectId].current == fundsToReclaim);

        (bool success,) = projects[projectId].creator.call{value : fundsToReclaim}("");
        require(success, "Transfer failed.");
        projects[projectId].status = ProjectStatuses.SUCCESS;

        return projects[projectId];
    }
}
