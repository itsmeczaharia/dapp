App = {
    web3Provider: null,
    contracts: {},
    categories: {},
    tags: {},
    accounts: [],
    userSet: false,
    projectStatuses: {
        0: 'CREATED',
        1: 'LIVE',
        2: 'FINISHED',
        3: 'SUCCESS',
        4: 'REFUNDED',
    },
    projectStatusesColors: {
        0: 'text-warning',
        1: 'text-success',
        2: 'text-primary',
        3: 'text-info',
        4: 'text-muted',
    },

    init: async function () {
        return await App.initWeb3();
    },

    initWeb3: async function () {
        // Modern dapp browsers...
        if (window.ethereum) {
            console.log(window.ethereum);
            App.web3Provider = window.ethereum;
            try {
                // Request account access
                await window.ethereum.enable();
                App.userSet = true;
            } catch (error) {
                // User denied account access...
                console.error("User denied account access")
            }
        }
        // Legacy dapp browsers...
        else if (window.web3) {
            App.web3Provider = window.web3.currentProvider;
        }
        // If no injected web3 instance is detected, fall back to Ganache
        else {
            App.web3Provider = new Web3.providers.HttpProvider('http://localhost:7545');
        }
        web3 = new Web3(App.web3Provider);

        App.accounts = await web3.eth.getAccounts();
        web3.eth.defaultAccount = App.accounts[0];

        if (App.userSet) {
            $(".overlay22").hide();
            $(".the-main").show();
        } else {
            return;
        }
        console.log(App.accounts[0]);
        return App.initContract();
    },

    initContract: function () {
        $.getJSON('/Dissertation.json', function (data) {
            // Get the necessary contract artifact file and instantiate it with @truffle/contract
            App.contracts.Dissertation = TruffleContract(data);

            // Set the provider for our contract
            App.contracts.Dissertation.setProvider(App.web3Provider);

            App.render();
            App.attachEvents();
        });
    },

    render: function () {
        let instance = "";
        App.contracts.Dissertation.deployed().then(function (i) {
            instance = i;
            return instance.getCategories();

        }).then(function (categories) {
            let categoryTemplate = $('#category-template');
            let categoriesList = $('#categories');
            let createProjectFormSelect = $("#create-project-modal form select");

            createProjectFormSelect.empty();
            categoriesList.empty();

            for (const category of categories) {
                App.categories[category.categoryId] = category; // cache the categories

                categoryTemplate.find('.category-name').text(category.categoryName);
                categoryTemplate.find('.category-name').attr('href', "/categories/" + category.categoryName.toLowerCase());
                categoryTemplate.find('.category-projects-num').text(category.projects.length);
                categoriesList.append(categoryTemplate.html());

                createProjectFormSelect.append("<option value=\"" + category.categoryId + "\">" + category.categoryName + "</option>")
            }

            return instance.getPopularProjects();

        }).then(function (projects) {
            let popularProjects = $(".most-popular-projects");
            let popularProjectTemplate = $(".popular-project-template");

            projects = projects.slice().sort(
                (a, b) => (parseInt(a[10]) > parseInt(b[10]) ? -1 : (parseInt(b[10]) > parseInt(a[10]) ? 1 : 0)));

            if (projects.length > 10) projects = projects.slice(0, 10);

            popularProjects.empty();
            for (const project of projects) {
                let launchDate = new Date(0);

                launchDate.setUTCSeconds(project.launchDate);

                popularProjectTemplate.find(".project-title").text(project.projectName);
                popularProjectTemplate.find("a").attr("href", "/projects/" + project.projectId);
                popularProjectTemplate.find("p").text((project.current * 100 / project.target).toFixed(1) + "% funded");
                popularProjectTemplate.find(".launch-date").html(
                    "Launched on <strong>" + launchDate.toISOString().replace(/T/, ' ').      // replace T with a space
                        replace(/ .+/, '') + "</strong>");
                popularProjectTemplate.find(".reactions").html("with <strong>" + project.totalReacts + "</strong> reactions");

                popularProjects.append(popularProjectTemplate.html());
            }
            return instance.getTags();

        }).then(function (tags) {
            let tagTemplate = $('#tag-template');
            let customTags = $('.custom-tags');

            customTags.empty();

            for (const tag of tags) {
                App.tags[tag.tagId] = tag; // cache the tags

                tagTemplate.find("a").text(tag.tagName);
                customTags.append(tagTemplate.html());
            }

            if (document.URL.includes("my-projects")) {
                App.renderMyProjects(instance);
            } else if (document.URL.includes("supported-projects")) {
                App.renderSupportedProjects(instance);
            } else if (document.URL.includes("projects")) {
                App.renderProject(instance);
            } else if (document.URL.includes("categories")) {
                App.renderCategory(instance);
            } else if (document.URL.includes("tags")) {
                App.renderTag(instance);
            } else {
                App.renderHome(instance);
            }
        });
    },

    renderGenericProjects: async function (projects, bigTitle) {
        console.log(projects);
        let projectTemplate = $('#project-template');
        let customProjects = $('.custom-projects');

        customProjects.empty();
        customProjects.append(bigTitle.html());

        projects = projects.slice().sort(
            (a, b) => (parseInt(a[0]) > parseInt(b[0]) ? -1 : (parseInt(b[0]) > parseInt(a[0]) ? 1 : 0)));

        for (const project of projects) {
            projectTemplate.find("h2 a").text(project.projectName);
            projectTemplate.find("p.card-text").text(project.projectDescription);

            let currentToEther = web3.utils.fromWei(project.current, 'ether');
            let targetToEther = web3.utils.fromWei(project.target, 'ether');

            // let percentage = Math.floor(Math.random() * 100)
            let progressPercentage = currentToEther / targetToEther * 100;
            let progress = currentToEther + " ETH / " + targetToEther + " ETH " +
                "<i class=\"fab fa-ethereum\" style='font-size: 1.25em;'></i>\n";
            projectTemplate.find("div.progress div").attr('style', "width: " + progressPercentage + "%;");
            projectTemplate.find("div.progress div").attr('aria-valuenow', progressPercentage);
            projectTemplate.find("h6.card-subtitle span").html(progress);
            projectTemplate.find(".reacts").text(project.totalReacts);
            projectTemplate.find(".backers span").text(project.backers);
            projectTemplate.find(".go-to-project").attr('onclick', "window.location='/projects/" + project.projectId + "'");

            customProjects.append(projectTemplate.html());
        }
        $(".loader").hide();
        customProjects.show();
    },

    renderMyProjects: async function (instance) {
        let projects = await instance.getUserProjects(App.accounts[0]);

        let bigTitle = $('#big-title');
        bigTitle.find("h2").text("My Projects");

        $(".nav .nav-link").removeClass("active").addClass("link-dark");
        $(".nav .nav-link[href=\"/my-projects\"]").addClass("active").removeClass("link-dark");


        await App.renderGenericProjects(projects, bigTitle);
    },

    renderSupportedProjects: async function (instance) {
        let projects = await instance.getUserSupportedProjects(App.accounts[0]);

        let bigTitle = $('#big-title');
        bigTitle.find("h2").text("Supported Projects");

        $(".nav .nav-link").removeClass("active").addClass("link-dark");
        $(".nav .nav-link[href=\"/supported-projects\"]").addClass("active").removeClass("link-dark");


        await App.renderGenericProjects(projects, bigTitle);
    },

    renderHome: async function (instance) {
        let projects = await instance.getAllProjects();

        let bigTitle = $('#big-title');
        bigTitle.find("h2").text("Recent Projects");

        $(".nav .nav-link").removeClass("active").addClass("link-dark");
        $(".nav .nav-link[href=\"/home\"]").addClass("active").removeClass("link-dark");

        await App.renderGenericProjects(projects, bigTitle);
    },
    renderProject: async function (instance) {
        let customProject = $('.custom-project');
        let supportProjectModal = $('#support-project');
        let reactButton = $(".react-to-project");
        let projectStatus = customProject.find(".project-status");

        $(".disabled").removeClass("disabled");
        projectStatus.removeClass();
        projectStatus.addClass("project-status");

        let projectId = document.URL.match(/projects\/(\d+)/)[1];

        let project = await instance.projects(projectId);
        let tags = await instance.getTagsForProject(projectId);
        let reacts = await instance.getReacts(projectId);
        let launchDate = new Date(0);

        launchDate.setUTCSeconds(project.launchDate);
        let currentDate = new Date().getTime() / 1000;
        let currentToEther = web3.utils.fromWei(project.current, 'ether');
        let targetToEther = web3.utils.fromWei(project.target, 'ether');

        // let percentage = Math.floor(Math.random() * 100)

        let progressPercentage = currentToEther / targetToEther * 100;
        let progress = currentToEther + " ETH / " + targetToEther + " ETH ( " + progressPercentage.toFixed(1) + "%) "
            + "<i class=\"fab fa-ethereum text-success\" style='font-size: 1.35em;'></i>";
        customProject.find(".card-subtitle span").html(progress);

        customProject.find("div.progress div").attr('style', "width: " + progressPercentage + "%;");

        customProject.find("div.progress div").attr('aria-valuenow', progressPercentage);
        customProject.find("h1").text(project.projectName);

        customProject.find(".launched-on").html("Project launched on: <strong> " + launchDate.toISOString().replace(/T/, ' ').      // replace T with a space
            replace(/\..+/, '') + "</strong>");
        customProject.find(".description").text(project.projectDescription);
        customProject.find(".project-status").text(App.projectStatuses[project.status]);
        customProject.find(".project-status").addClass(App.projectStatusesColors[project.status]);
        customProject.find(".project-deadline").text(((project.deadline - currentDate) / (3600 * 24)).toFixed(2));
        customProject.find(".project-supporters").text(project.backers);
        customProject.find(".cats a").text(App.categories[project.category].categoryName);
        customProject.find(".project-reacts").text(reacts.length);

        if (reacts.includes(App.accounts[0])) {
            reactButton.addClass("active");
            reactButton.attr("aria-pressed", "true");
            reactButton.find("span").text("Unlike");
        } else {
            reactButton.removeClass("active");
            reactButton.removeAttr("aria-pressed");
            reactButton.find("span").text("Like");
        }
        if ([0, 2].includes(parseInt(project.status))) {

            $('.change-project').text("Mark Project as LIVE");
        } else {
            $('.change-project').text("Finish Project and reclaim funds.");
        }
        let tempTagList = $(".tags");

        tempTagList.empty();
        for (let tag of tags) {
            tempTagList.append("<li><u><a href=\"#\">" + tag + "</a></u></li>")
        }
        supportProjectModal.find("h5").text(project.projectName);
        if (App.accounts[0] === project.creator) {

            $(".admin-project").show();
        }
        console.log(project)
        console.log(parseInt(project.status));
        console.log(parseInt(project.deadline))
        console.log(new Date().getTime() / 1000)

        if (parseInt(project.status) === 0) {
            $("button[data-bs-target=\"#support-project\"]").addClass("disabled");
            $(".cancel-project").addClass("disabled");
        }

        if (parseInt(project.status) === 1) {
            if ((new Date().getTime() / 1000) < parseInt(project.deadline)) {
                console.log("here");
                $(".change-project").addClass("disabled");
            }
        }

        if (parseInt(project.status) === 3) {
            $("button[data-bs-target=\"#support-project\"]").addClass("disabled");
            $(".cancel-project").addClass("disabled");
            $(".change-project").addClass("disabled");
        }

        if (parseInt(project.status) === 4) {
            $("button[data-bs-target=\"#support-project\"]").addClass("disabled");
            $(".cancel-project").addClass("disabled");
            $(".change-project").addClass("disabled");
            reactButton.addClass("disabled");
        }

        $(".loader").hide();
        customProject.show();
    },
    renderCategory: async function (instance) {

        let categoryId;
        let categoryName = document.URL.match(/categories\/(\w+)/)[1];
        for (const [key, value] of Object.entries(App.categories)) {
            if (value.categoryName.toLowerCase() === categoryName) {
                categoryId = key;
                break;
            }
        }
        let projects = await instance.getCategoryProjects(categoryId);
        let projectTemplate = $('#project-template');
        let customProjects = $('.custom-projects');

        customProjects.empty();

        let bigTitle = $('#big-title');
        bigTitle.find("h2").text(App.categories[categoryId].categoryName + " Category");
        customProjects.append(bigTitle.html());

        projects = projects.slice().sort(
            (a, b) => (parseInt(a[0]) > parseInt(b[0]) ? -1 : (parseInt(b[0]) > parseInt(a[0]) ? 1 : 0)));

        await App.renderGenericProjects(projects, bigTitle);
    },
    renderTag: async function (instance) {
    },

    attachEvents: function () {

        $(".create-project-button").click(function () {
            console.log("asdas");
            App.contracts.Dissertation.deployed().then(async function (instance) {

                let projectName = $("#inputName").val();
                let projectDescription = $("#inputDetails").val();
                let deadline = new Date($("#inputDate").val()).getTime() / 1000;
                let target = web3.utils.toWei($("#inputTarget").val(), 'ether');
                let category = $("#inputCategory").val();

                let projectId = await instance.createProject.call(
                    projectName, projectDescription, deadline, target, category, {'from': App.accounts[0]});

                await instance.createProject(
                    projectName, projectDescription, deadline, target, category, {'from': App.accounts[0]});

                return projectId;

            }).then(function (projectId) {
                window.location.href = "/projects/" + projectId
            });
        });

        $(".support-me").click(function () {
            App.contracts.Dissertation.deployed().then(function (instance) {

                let projectId = document.URL.match(/projects\/(\d+)/)[1];
                let amount = $('#amount-support').val();
                amount = web3.utils.toWei(amount, 'ether')

                return instance.supportProject(
                    projectId, {'from': App.accounts[0], 'value': amount});

            }).then(function () {
                $('#support-project').modal('toggle');
                App.render();
            });
        });

        $(".react-to-project").click(function () {

            App.contracts.Dissertation.deployed().then(async function (instance) {

                let projectId = document.URL.match(/projects\/(\d+)/)[1];
                let reacts = await instance.getReacts(projectId);

                if (reacts.includes(App.accounts[0])) {
                    await instance.removeReact(projectId, {'from': App.accounts[0]})
                } else {
                    await instance.addReact(projectId, {'from': App.accounts[0]})
                }

            }).then(function () {
                App.render();
            });

            $(".react-to-project").removeAttr("aria-pressed");
        });

        $(".delete-project").click(function () {

            App.contracts.Dissertation.deployed().then(async function (instance) {
                let projectId = document.URL.match(/projects\/(\d+)/)[1];
                await instance.deleteProject(projectId, {'from': App.accounts[0]});
            }).then(function () {
                App.render();
            });

        });

        $(".cancel-project").click(function () {

            App.contracts.Dissertation.deployed().then(async function (instance) {
                let projectId = document.URL.match(/projects\/(\d+)/)[1];
                await instance.refundFunds(projectId, {'from': App.accounts[0]});
            }).then(function () {
                App.render();
            });

        });

        $(".change-project").click(function () {

            App.contracts.Dissertation.deployed().then(async function (instance) {
                let projectId = document.URL.match(/projects\/(\d+)/)[1];
                let project = await instance.projects(projectId);
                let status;

                if (parseInt(project.status) === 0) {
                    await instance.changeProjectStatus(projectId, 1, {'from': App.accounts[0]});
                } else {
                    await instance.reclaimFunds(projectId, {'from': App.accounts[0]});

                }

            }).then(function () {
                App.render();
            });

        });

        $(".logout").click(async function () {

            App.contracts.Dissertation.deployed().then(async function (instance) {
                let projectId = document.URL.match(/projects\/(\d+)/)[1];
                let project = await instance.reclaimFunds(projectId, {'from': App.accounts[0]});
            }).then(function () {
                App.render();
            });
        });
    },
};


$(window).on('load', function () {
    App.init();
});
