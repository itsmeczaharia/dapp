const express = require('express');
const app = express();
const router = express.Router();

const port = 3000;

app.use(express.static('src'));
app.use(express.static('build/contracts'))

app.use(router);

app.get('/home', (req, res) => {
    res.sendFile(__dirname + '/src/index.html');
});

app.get('/my-projects', (req, res) => {
    res.sendFile(__dirname + '/src/index.html');
});

app.get('/supported-projects', (req, res) => {
    res.sendFile(__dirname + '/src/index.html');
});

app.get('/projects/:projectId', (req, res) => {
    res.sendFile(__dirname + '/src/index.html');
});

app.get('/categories/:category', (req, res) => {
    res.sendFile(__dirname + '/src/index.html');
});

app.get('/tags/:tag', (req, res) => {
    res.sendFile(__dirname + '/src/index.html');
});

app.listen(port, () => {
    console.log(`Example app listening on port ${port}!`)
});

module.exports = app;